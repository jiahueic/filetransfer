package Client;
import java.net.*;
import java.io.*;
import java.nio.ByteBuffer;


public class Client {
    public static void main(String[] args) {
        Socket clientSocket = null;
        try{
            clientSocket = new Socket("localhost",4333);
            // use readlink -f filename to find file in Ubuntu terminal
            String filename = "/home/jiahueic/Downloads/pic1.jpg";
            File myFile = new File(filename);
            System.out.println(myFile.exists());
            // create random access file read-only
            RandomAccessFile targetFile = new RandomAccessFile(filename,"r");
            // returns the length of the file, measured in bytes
            int fileSize = (int)targetFile.length();
            // get output stream returns an output stream for writing bytes to this socket
            // An Input/ Output error occurs as this socket is closed
            OutputStream out = clientSocket.getOutputStream();
            InputStream in = clientSocket.getInputStream();
            // read from the random access file from a certain position with a predefined buffer size
            // before commencing with file sending, send the size of the target file first
            System.out.println("The file size of target file is: " + fileSize);
            out.write(ByteBuffer.allocate(4).putInt(fileSize).array());
            byte[] recogBuff = new byte[1000];
            in.read(recogBuff);
            // convert recognition byte array into String
            System.out.println("Server says: " + new String(recogBuff));
            long pos = 0;
            while(pos < fileSize){
                // initialize the byte array that will be returned from reading the random access file
                byte[] bytesToRead = new byte[1000];
                targetFile.seek(pos);
                targetFile.read(bytesToRead);
                // write the bytes array read from the random access file
                out.write(bytesToRead);
                pos += 1000;
            }
            System.out.println("File transfer point is reached");
            // remember to close the random access file after the transfer is complete
            targetFile.close();

        }
        catch(UnknownHostException e){
            System.out.println("Client Host: " + e.getMessage());
        }
        catch(IOException e){
            e.printStackTrace();
        }finally {
            // close the client socket after everything is complete
            if(clientSocket != null){
                try{
                    clientSocket.close();
                }
                catch(IOException e){
                    e.printStackTrace();
                }
            }
        }
    }



}
