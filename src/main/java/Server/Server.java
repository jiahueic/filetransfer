package Server;
import java.net.*;
import java.io.*;
public class Server {
    public static void main(String[] args) {
        try{
            ServerSocket serverSocket = new ServerSocket(4333);
            int i = 0;
           while(true){
               System.out.println("Server listening on: " + serverSocket.getInetAddress() + ":" + serverSocket.getLocalPort());
               Socket client = serverSocket.accept();
               // establish new Connection
               Connection c = new Connection(client);
               i++;
               System.out.println("Received connection " + i + " from"+client.getInetAddress()+":"+client.getPort());

           }
        }
        catch(IOException e){
            e.printStackTrace();
        }


    }
}
