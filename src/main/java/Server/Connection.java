package Server;

import java.nio.ByteBuffer;
import java.net.*;
import java.io.*;


public class Connection extends Thread{
    // how does the server save the bytes of the file read bit by bit?
    InputStream in;
    OutputStream out;
    Socket clientSocket;
    public Connection(Socket clientSocket){
        try{
            this.clientSocket = clientSocket;
            in = clientSocket.getInputStream();
            out = clientSocket.getOutputStream();
            this.start();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
    public void run(){
        // since the file is to be transferred from client to server, we should create a new file with
        // the same filename
        try{
            File newFile = new File("/home/jiahueic/Downloads/pic1transferred.jpg");
            boolean created = newFile.createNewFile();
            System.out.println("The new file is created: " + created);
            RandomAccessFile fileTransferred = new RandomAccessFile(newFile,"rw");
            // receive the filesize from the client
            byte[] fileSizeInput = new byte[4];
            in.read(fileSizeInput);
            // need to send recognition to client that filesize is received
            String recogMess = "Filesize received";
            out.write(recogMess.getBytes());
            int targetFileSize = ByteBuffer.wrap(fileSizeInput).getInt();
            System.out.println("Target file size is: " + targetFileSize);
            long pos = 0;
            while(pos < targetFileSize){
                // initialise a buffer array to store the data obtained from input stream
                byte[] bufferInput = new byte[1000];
                in.read(bufferInput);
                // write the bufferInput into the RandomAccessFile
                fileTransferred.seek(pos);
                fileTransferred.write(bufferInput);
                pos += 1000;
            }
            // remember to close the fileTransferred after writing everything
            fileTransferred.close();
            // server needs to tell client that the file is received
            String finalMess = "Target file successfully transferred";
            out.write(finalMess.getBytes());

        }
        catch(IOException e){
            e.printStackTrace();
        }


    }
}
